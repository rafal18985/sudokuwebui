export abstract class BoardSizes {
  static readonly size = 9;
  static readonly squareSize = BoardSizes.size / 3;
}
