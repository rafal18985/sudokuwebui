import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Board } from '../models/board';
import { Position } from '../models/position';

export interface SudokuStateModel {
  board: Board;
  selectedPosition: Position;
}

@Injectable({
  providedIn: 'root',
})
export class SudokuState {
  state$: Observable<SudokuStateModel>;
  private state: BehaviorSubject<SudokuStateModel>;

  constructor() {
    this.state = new BehaviorSubject({ board: null, selectedPosition: null, elapsedTime: 0 });
    this.state$ = this.state.asObservable();
  }

  getState() {
    return this.state.getValue();
  }

  setState(state: SudokuStateModel) {
    this.state.next(state);
  }
}
