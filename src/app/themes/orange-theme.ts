import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const orangeTheme: Theme = {
  ...baseLightTheme,
  name: 'Orange',

  // grid
  selectedCellBorderColor: '#FB8C00', // orange600

  // fontColor
  defaultFontColor: '#FB8C00', // orange600
  selectedCellFontColor: '#FB8C00', // orange600
  selectedAreaFontColor: '#FB8C00', // orange600

  // background
  selectedValueBackgroundColor: '#FB8C00', // orange600
  selectedAreaBackgroundColor: '#ffe9c9', // between orange50 & orange100
};
