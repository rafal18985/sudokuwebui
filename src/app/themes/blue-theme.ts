import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const blueTheme: Theme = {
  ...baseLightTheme,
  name: 'Blue',

  // grid
  selectedCellBorderColor: '#1E88E5', // blue600

  // fontColor
  defaultFontColor: '#1E88E5', // blue600
  selectedCellFontColor: '#1E88E5', // blue600
  selectedAreaFontColor: '#1E88E5', // blue600

  // background
  selectedValueBackgroundColor: '#1E88E5', // blue600
  selectedAreaBackgroundColor: '#d7ecfc', // between blue50 & blue100
};
