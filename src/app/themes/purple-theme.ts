import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const purpleTheme: Theme = {
  ...baseLightTheme,
  name: 'Purple',

  // grid
  selectedCellBorderColor: '#AB47BC', // purple400

  // fontColor
  defaultFontColor: '#AB47BC', // purple400
  selectedCellFontColor: '#AB47BC', // purple400
  selectedAreaFontColor: '#AB47BC', // purple400

  // background
  selectedValueBackgroundColor: '#AB47BC', // purple400
  selectedAreaBackgroundColor: '#ead2ee', // between purple50 & purple100
};
