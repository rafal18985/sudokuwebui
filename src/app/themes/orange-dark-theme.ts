import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const orangeDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Orange dark',

  // grid
  selectedCellBorderColor: '#FF9800', // orange500

  // fontColor
  defaultFontColor: '#FF9800', // orange500
  selectedCellFontColor: '#FF9800', // orange500

  // background
  selectedValueBackgroundColor: '#FF9800', // orange500
};
