import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const blueDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Blue dark',

  // grid
  selectedCellBorderColor: '#2196F3', // blue500

  // fontColor
  defaultFontColor: '#2196F3', // blue500
  selectedCellFontColor: '#2196F3', // blue500

  // background
  selectedValueBackgroundColor: '#2196F3', // blue500
};
