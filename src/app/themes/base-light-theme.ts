import { Theme } from '../models/theme';

export const baseLightTheme: Omit<
  Theme,
  | 'name'
  | 'selectedCellBorderColor'
  | 'defaultFontColor'
  | 'selectedCellFontColor'
  | 'selectedAreaFontColor'
  | 'selectedValueBackgroundColor'
  | 'selectedAreaBackgroundColor'
> = {
  // grid
  padding: 4,
  thinLineWidth: 1,
  thinLineColor: '#424242', // grey800
  thickLineWidth: 3,
  thickLineColor: '#212121', // grey900
  selectedCellLineWidth: 4,
  selectedCellBorderRadius: 6,

  // font
  fontFamily: 'Roboto',
  valueFontSizeFactor: 0.7,
  noteFontSizeFactor: 0.28,

  // fontColor
  lockedCellFontColor: '#212121', // grey900
  selectedValueFontColor: '#FAFAFA', // grey50
  mistakeFontColor: '#FAFAFA', // grey50

  // background
  defaultBackgroundColor: '#FAFAFA', // grey50
  lockedCellBackgroundColor: '#E0E0E0', // grey300
  selectedCellBackgroundColor: '#FAFAFA', // grey50
  mistakeBackgroundColor: '#f44336', // red500
};
