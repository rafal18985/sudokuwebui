import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const pinkDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Pink dark',

  // grid
  selectedCellBorderColor: '#F06292', // pink300

  // fontColor
  defaultFontColor: '#F06292', // pink300
  selectedCellFontColor: '#F06292', // pink300

  // background
  selectedValueBackgroundColor: '#F06292', // pink300
};
