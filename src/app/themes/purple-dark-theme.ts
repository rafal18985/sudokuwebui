import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const purpleDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Purple dark',

  // grid
  selectedCellBorderColor: '#BA68C8', // purple300

  // fontColor
  defaultFontColor: '#BA68C8', // purple300
  selectedCellFontColor: '#BA68C8', // purple300

  // background
  selectedValueBackgroundColor: '#BA68C8', // purple300
};
