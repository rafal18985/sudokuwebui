import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const greenTheme: Theme = {
  ...baseLightTheme,
  name: 'Green',

  // grid
  selectedCellBorderColor: '#4CAF50', // green500

  // fontColor
  defaultFontColor: '#4CAF50', // green500
  selectedCellFontColor: '#4CAF50', // green500
  selectedAreaFontColor: '#4CAF50', // green500

  // background
  selectedValueBackgroundColor: '#4CAF50', // green500
  selectedAreaBackgroundColor: '#d8eed9', // between green50 & green100
};
