import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const greenDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Green dark',

  // grid
  selectedCellBorderColor: '#66BB6A', // green400

  // fontColor
  defaultFontColor: '#66BB6A', // green400
  selectedCellFontColor: '#66BB6A', // green400

  // background
  selectedValueBackgroundColor: '#66BB6A', // green400
};
