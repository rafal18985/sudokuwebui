import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const baseDarkTheme: Omit<
  Theme,
  'name' | 'selectedCellBorderColor' | 'defaultFontColor' | 'selectedCellFontColor' | 'selectedValueBackgroundColor'
> = {
  ...baseLightTheme,

  // grid
  thinLineColor: '#E0E0E0', // grey300
  thickLineColor: '#F5F5F5', // grey100

  // fontColor
  lockedCellFontColor: '#FAFAFA', // grey50
  selectedValueFontColor: '#212121', // grey900
  selectedAreaFontColor: '#FAFAFA', // grey50
  mistakeFontColor: '#FAFAFA', // grey50

  // background
  defaultBackgroundColor: '#212121', // grey900
  lockedCellBackgroundColor: '#424242', // grey700
  selectedCellBackgroundColor: '#212121', // grey900
  selectedAreaBackgroundColor: '#616161', // grey600
  mistakeBackgroundColor: '#e53935', // red600
};
