import { Theme } from '../models/theme';
import { baseLightTheme } from './base-light-theme';

export const pinkTheme: Theme = {
  ...baseLightTheme,
  name: 'Pink',

  // grid
  selectedCellBorderColor: '#F06292', // pink300

  // fontColor
  defaultFontColor: '#F06292', // pink300
  selectedCellFontColor: '#F06292', // pink300
  selectedAreaFontColor: '#F06292', // pink300

  // background
  selectedValueBackgroundColor: '#F06292', // pink300
  selectedAreaBackgroundColor: '#fbdde8', // between pink50 & pink100
};
