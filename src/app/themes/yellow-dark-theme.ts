import { Theme } from '../models/theme';
import { baseDarkTheme } from './base-dark-theme';

export const yellowDarkTheme: Theme = {
  ...baseDarkTheme,
  name: 'Yellow dark',

  // grid
  selectedCellBorderColor: '#FFF176', // yellow300

  // fontColor
  defaultFontColor: '#FFF176', // yellow300
  selectedCellFontColor: '#FFF176', // yellow300

  // background
  selectedValueBackgroundColor: '#FFF176', // yellow300
};
