import { blueDarkTheme } from './blue-dark-theme';
import { blueTheme } from './blue-theme';
import { greenDarkTheme } from './green-dark-theme';
import { greenTheme } from './green-theme';
import { orangeDarkTheme } from './orange-dark-theme';
import { orangeTheme } from './orange-theme';
import { pinkDarkTheme } from './pink-dark-theme';
import { pinkTheme } from './pink-theme';
import { purpleDarkTheme } from './purple-dark-theme';
import { purpleTheme } from './purple-theme';
import { yellowDarkTheme } from './yellow-dark-theme';

export const themes = [
  blueTheme,
  blueDarkTheme,
  orangeTheme,
  orangeDarkTheme,
  pinkTheme,
  pinkDarkTheme,
  purpleTheme,
  purpleDarkTheme,
  greenTheme,
  greenDarkTheme,
  yellowDarkTheme,
];
