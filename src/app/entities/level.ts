export interface Level {
  id: number;
  packId: number;
  unsolvedData: string;
  solution: string;
  elapsedTime?: number;
  currentState?: string;
  solvedPercent?: number;
}
