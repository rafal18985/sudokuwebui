import { DifficultyLevel } from '../models/difficulty-level';

export interface Pack {
  id: number;
  difficultyLevel: DifficultyLevel;
}
