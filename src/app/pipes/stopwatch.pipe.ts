import { Pipe, PipeTransform } from '@angular/core';
import { Stopwatch } from '../utils/stopwatch';

@Pipe({
  name: 'stopwatch',
})
export class StopwatchPipe implements PipeTransform {
  transform(elapsed: number): string {
    const hours = Math.trunc(elapsed / Stopwatch.millisPerHour);
    const minutes = Math.trunc(elapsed / Stopwatch.millisPerMinute) % 60;
    const seconds = Math.trunc(elapsed / Stopwatch.millisPerSecond) % 60;

    if (hours > 0) {
      return `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    } else {
      return `${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
    }
  }
}
