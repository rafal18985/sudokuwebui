import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AngularResizedEventModule } from 'angular-resize-event';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardCanvasComponent } from './components/board-canvas/board-canvas.component';
import { CongratulationDialogComponent } from './components/congratulation-dialog/congratulation-dialog.component';
import { DigitPadComponent } from './components/digit-pad/digit-pad.component';
import { HeightDependentFontSizeComponent } from './components/height-dependent-font-size/height-dependent-font-size.component';
import { KeepAspectRatioComponent } from './components/keep-aspect-ratio/keep-aspect-ratio.component';
import { DifficultyLevelSelectionComponent } from './components/menu/difficulty-level-selection/difficulty-level-selection.component';
import { LevelSelectionComponent } from './components/menu/level-selection/level-selection.component';
import { MenuComponent } from './components/menu/menu.component';
import { PackSelectionComponent } from './components/menu/pack-selection/pack-selection.component';
import { OptionsDialogComponent } from './components/options-dialog/options-dialog.component';
import { SudokuComponent } from './components/sudoku/sudoku.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { StopwatchPipe } from './pipes/stopwatch.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SudokuComponent,
    DigitPadComponent,
    MenuComponent,
    LevelSelectionComponent,
    StopwatchPipe,
    KeepAspectRatioComponent,
    HeightDependentFontSizeComponent,
    ToolbarComponent,
    BoardCanvasComponent,
    OptionsDialogComponent,
    DifficultyLevelSelectionComponent,
    PackSelectionComponent,
    CongratulationDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularResizedEventModule,
    MatDialogModule,
    MatSlideToggleModule,
    FormsModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatListModule,
    MatProgressBarModule,
    MatMenuModule,
  ],
  entryComponents: [OptionsDialogComponent, CongratulationDialogComponent],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
