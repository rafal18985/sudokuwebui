export interface Cell {
  value: number;
  notes: boolean[];

  locked: boolean;
}
