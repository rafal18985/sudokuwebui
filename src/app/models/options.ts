export interface Options {
  highlightSolutionMistake: boolean;
  highlightDuplicateMistake: boolean;
  highlightAvailableDigits: boolean;
  highlightSelectedValues: boolean;
  highlightSelectedArea: boolean;
  shouldRemoveNotesFromSelectedCellArea: boolean;
  selectedThemeIndex: number;
}
