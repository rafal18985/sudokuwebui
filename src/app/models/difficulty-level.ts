export enum DifficultyLevel {
  EASY,
  NORMAL,
  HARD,
  INSANE,
}

export function difficultyLevelToString(difficultyLevel: DifficultyLevel) {
  switch (difficultyLevel) {
    case DifficultyLevel.EASY:
      return 'easy';
      break;
    case DifficultyLevel.NORMAL:
      return 'normal';
      break;
    case DifficultyLevel.HARD:
      return 'hard';
      break;
    case DifficultyLevel.INSANE:
      return 'insane';
      break;
  }
}

export function difficultyLevelToEnum(difficultyLevelString: string) {
  switch (difficultyLevelString) {
    case 'easy':
      return DifficultyLevel.EASY;
      break;
    case 'normal':
      return DifficultyLevel.NORMAL;
      break;
    case 'hard':
      return DifficultyLevel.HARD;
      break;
    case 'insane':
      return DifficultyLevel.INSANE;
      break;
  }
}
