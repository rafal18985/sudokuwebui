import { Cell } from './cell';

export interface Board {
  cells: Cell[][];
  solution: number[][];
}
