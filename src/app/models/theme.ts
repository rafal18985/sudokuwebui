export interface Theme {
  name: string;

  // grid
  padding: number;
  thinLineWidth: number;
  thinLineColor: string;
  thickLineWidth: number;
  thickLineColor: string;
  selectedCellLineWidth: number;
  selectedCellBorderColor: string;
  selectedCellBorderRadius: number;

  // font
  fontFamily: string;
  valueFontSizeFactor: number;
  noteFontSizeFactor: number;

  // fontColor
  defaultFontColor: string;
  lockedCellFontColor: string;
  selectedCellFontColor: string;
  selectedValueFontColor: string;
  selectedAreaFontColor: string;
  mistakeFontColor: string;

  // background
  defaultBackgroundColor: string;
  lockedCellBackgroundColor: string;
  selectedCellBackgroundColor: string;
  selectedValueBackgroundColor: string;
  selectedAreaBackgroundColor: string;
  mistakeBackgroundColor: string;
}
