import { makepuzzle, solvepuzzle } from 'sudoku';
import { BoardSizes } from '../constants/board-sizes';
import { Board } from '../models/board';

export class SudokuGenerator {
  generate(): Board {
    const raw = makepuzzle();
    const solution: number[] = solvepuzzle(raw);

    const board: Board = { cells: [], solution: [] };

    for (let row = 0; row < BoardSizes.size; row++) {
      board.cells[row] = [];
      board.solution[row] = [];
      for (let col = 0; col < BoardSizes.size; col++) {
        board.cells[row][col] = { value: null, notes: Array<boolean>(BoardSizes.size).fill(false), locked: false };
      }
    }

    for (let i = 0; i < raw.length; i++) {
      const value = raw[i];
      const row = Math.trunc(i / BoardSizes.size);
      const col = i % BoardSizes.size;

      if (value !== null) {
        const cell = board.cells[row][col];
        cell.value = value + 1;
        cell.locked = true;
      }

      board.solution[row][col] = solution[i] + 1;
    }

    return board;
  }

  generateString() {
    const raw: number[] = makepuzzle();
    const solution: number[] = solvepuzzle(raw);

    const unsolvedString = raw
      .map((rawValue) => {
        if (rawValue === null) {
          return 0;
        } else {
          return rawValue + 1;
        }
      })
      .join('');

    const solutionString = solution.map((value) => value + 1).join('');

    return { unsolvedString, solutionString };
  }
}
