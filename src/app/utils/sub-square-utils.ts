import { Position } from '../models/position';

export abstract class SubSquareUtils {
  public static calculateSubSquarePosition(position: Position, subSquareRowCount: number, subSquareColCount: number): Position {
    return { row: Math.trunc(position.row / subSquareRowCount), col: Math.trunc(position.col / subSquareColCount) };
  }

  public static inSameSubSquare(position1: Position, position2: Position, subSquareRowCount: number, subSquareColCount: number): boolean {
    const position1SubSquare = this.calculateSubSquarePosition(position1, subSquareRowCount, subSquareColCount);
    const position2SubSquare = this.calculateSubSquarePosition(position2, subSquareRowCount, subSquareColCount);

    return position1SubSquare.row === position2SubSquare.row && position1SubSquare.col === position2SubSquare.col;
  }
}
