import { Position } from '../models/position';

export abstract class Array2dUtils {
  public static generatePositionsForRow(row: number, colCount: number): Position[] {
    const positions: Position[] = [];
    for (let col = 0; col < colCount; col++) {
      positions.push({ row, col });
    }

    return positions;
  }

  public static generatePositionsForCol(col: number, rowCount: number): Position[] {
    const positions: Position[] = [];
    for (let row = 0; row < rowCount; row++) {
      positions.push({ row, col });
    }

    return positions;
  }

  public static generatePositionsForSubSquare(
    subSquareRow: number,
    subSquareCol: number,
    subSquareRowCount: number,
    subSquareColCount: number
  ): Position[] {
    const positions: Position[] = [];
    for (let row = 0; row < subSquareRowCount; row++) {
      for (let col = 0; col < subSquareColCount; col++) {
        positions.push({ row: subSquareRow * subSquareColCount + row, col: subSquareCol * subSquareRowCount + col });
      }
    }

    return positions;
  }

  public static runForRow(callback: (rowPositions: Position[]) => void, row: number, rowCount: number, colCount: number): void {
    const rowPositions = Array2dUtils.generatePositionsForRow(row, colCount);
    callback(rowPositions);
  }

  public static runForCol(callback: (colPositions: Position[]) => void, col: number, rowCount: number, colCount: number): void {
    const colPositions = Array2dUtils.generatePositionsForCol(col, colCount);
    callback(colPositions);
  }

  public static runForSubSquare(
    callback: (subSquarePositions: Position[]) => void,
    subSquareRow: number,
    subSquareCol: number,
    subSquareRowCount: number,
    subSquareColCount: number
  ): void {
    const subSquarePositions = this.generatePositionsForSubSquare(subSquareRow, subSquareCol, subSquareRowCount, subSquareColCount);
    callback(subSquarePositions);
  }

  public static runForEachRow(callback: (rowPositions: Position[]) => void, rowCount: number, colCount: number): void {
    for (let row = 0; row < rowCount; row++) {
      Array2dUtils.runForRow(callback, row, rowCount, colCount);
    }
  }

  public static runForEachCol(callback: (colPositions: Position[]) => void, rowCount: number, colCount: number): void {
    for (let col = 0; col < colCount; col++) {
      Array2dUtils.runForCol(callback, col, rowCount, colCount);
    }
  }

  public static runForEachSubSquare(
    callback: (subSquarePositions: Position[]) => void,
    rowCount: number,
    colCount: number,
    subSquareRowCount: number,
    subSquareColCount: number
  ): void {
    for (let subSquareRow = 0; subSquareRow < rowCount / subSquareRowCount; subSquareRow++) {
      for (let subSquareCol = 0; subSquareCol < colCount / subSquareColCount; subSquareCol++) {
        Array2dUtils.runForSubSquare(callback, subSquareRow, subSquareCol, subSquareRowCount, subSquareColCount);
      }
    }
  }

  public static runForEachPosition(callback: (position: Position) => void, positions: Position[]): void {
    for (const position of positions) {
      callback(position);
    }
  }

  public static runRowWise(callback: (position: Position) => void, rowCount: number, colCount: number): void {
    Array2dUtils.runForEachRow((rowPositions) => Array2dUtils.runForEachPosition(callback, rowPositions), rowCount, colCount);
  }

  public static runColWise(callback: (position: Position) => void, rowCount: number, colCount: number): void {
    Array2dUtils.runForEachCol((colPositions) => Array2dUtils.runForEachPosition(callback, colPositions), rowCount, colCount);
  }

  public static runSubSquareWise(
    callback: (position: Position) => void,
    rowCount: number,
    colCount: number,
    subSquareRowCount: number,
    subSquareColCount: number
  ): void {
    Array2dUtils.runForEachSubSquare(
      (subSquarePositions) => Array2dUtils.runForEachPosition(callback, subSquarePositions),
      rowCount,
      colCount,
      subSquareRowCount,
      subSquareColCount
    );
  }
}
