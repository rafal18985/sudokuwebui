import { ICommand } from '../commands/i-command';

export class UndoRedoStack<T> {
  private undoStack: ICommand<T>[] = [];
  private redoStack: ICommand<T>[] = [];

  addAndDo(command: ICommand<T>, target: T) {
    command.do(target);
    if (command.canUndo()) {
      this.undoStack.push(command);
      this.redoStack = [];
    }
  }

  undo(target: T) {
    if (!this.canUndo()) {
      return;
    }

    const command = this.undoStack.pop();
    this.redoStack.push(command);
    command.undo(target);
  }

  redo(target: T) {
    if (!this.canRedo()) {
      return;
    }
    const command = this.redoStack.pop();
    this.undoStack.push(command);
    command.do(target);
  }

  canUndo() {
    return this.undoStack.length > 0;
  }

  canRedo() {
    return this.redoStack.length > 0;
  }
}
