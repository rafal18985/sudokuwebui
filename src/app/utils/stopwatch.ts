import { timer, Subscription } from 'rxjs';

export class Stopwatch {
  static readonly millisPerSecond = 1000;
  static readonly millisPerMinute = Stopwatch.millisPerSecond * 60;
  static readonly millisPerHour = Stopwatch.millisPerMinute * 60;

  interval = 1000;

  private elapsed = 0;
  private lastUpdateTime: number = null;
  private subscription: Subscription = null;

  public start(): void {
    if (this.isRunning()) {
      return;
    }
    this.lastUpdateTime = Date.now();

    this.subscription = timer(0, this.interval).subscribe((_) => {
      const now = Date.now();
      this.elapsed += now - this.lastUpdateTime;
      this.lastUpdateTime = now;
    });
  }

  public pause(): void {
    if (!this.isRunning()) {
      return;
    }

    const now = Date.now();
    this.elapsed += now - this.lastUpdateTime;
    this.lastUpdateTime = null;
    this.subscription?.unsubscribe();
    this.subscription = null;
  }

  public reset(): void {
    this.elapsed = 0;
    this.lastUpdateTime = null;
    this.subscription?.unsubscribe();
    this.subscription = null;
  }

  public restart(): void {
    this.reset();
    this.start();
  }

  public addTime({
    hours = 0,
    minutes = 0,
    seconds = 0,
    milliseconds = 0,
  }: {
    hours?: number;
    minutes?: number;
    seconds?: number;
    milliseconds?: number;
  } = {}): void {
    this.elapsed +=
      hours * Stopwatch.millisPerHour + minutes * Stopwatch.millisPerMinute + seconds * Stopwatch.millisPerSecond + milliseconds;
  }

  public get totalMinutes(): number {
    return Math.trunc(this.elapsed / Stopwatch.millisPerMinute);
  }
  public get totalSeconds(): number {
    return Math.trunc(this.elapsed / Stopwatch.millisPerSecond);
  }
  public get totalMilliseconds(): number {
    return this.elapsed;
  }

  public get hours(): number {
    return Math.trunc(this.elapsed / Stopwatch.millisPerHour);
  }
  public get minutes(): number {
    return this.totalMinutes % 60;
  }
  public get seconds(): number {
    return this.totalSeconds % 60;
  }
  public get milliseconds(): number {
    return this.totalMilliseconds % Stopwatch.millisPerSecond;
  }

  private isRunning(): boolean {
    return this.subscription !== null;
  }
}
