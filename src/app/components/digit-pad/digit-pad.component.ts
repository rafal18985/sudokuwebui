import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { Board } from 'src/app/models/board';
import { Cell } from 'src/app/models/cell';
import { Options } from 'src/app/models/options';
import { Position } from 'src/app/models/position';
import { DigitAvailability } from 'src/app/validators/digit-pad/digit-availability';
import { UsedDigits } from 'src/app/validators/digit-pad/used-digits';

@Component({
  selector: 'app-digit-pad',
  templateUrl: './digit-pad.component.html',
  styleUrls: ['./digit-pad.component.scss'],
})
export class DigitPadComponent implements OnChanges {
  @Input()
  board: Board;
  @Input()
  selectedPosition: Position;
  @Input()
  options: Options;
  @Input()
  forceNoteIfEmpty = false;

  @Output()
  digitClicked: EventEmitter<number> = new EventEmitter<number>();
  @Output()
  toogleNotesClicked = new EventEmitter();
  @Output()
  autoFillNotesClicked = new EventEmitter();
  @Output()
  clearCellClicked = new EventEmitter();

  digitAvailability = new DigitAvailability();
  usedDigits = new UsedDigits();

  selectedCell: Cell = null;
  selectedValue: number = null;
  isSelectedLocked: boolean = null;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedPosition || changes.board) {
      this.digitAvailability.update(this.board, this.selectedPosition);
      this.usedDigits.update(this.board);
      if (this.selectedPosition !== null) {
        this.selectedCell = this.board.cells[this.selectedPosition.row][this.selectedPosition.col];
        this.selectedValue = this.selectedCell.value;
        this.isSelectedLocked = this.selectedCell.locked;
      } else {
        this.selectedCell = null;
        this.selectedValue = null;
        this.isSelectedLocked = null;
      }
    }
  }
}
