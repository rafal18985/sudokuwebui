import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ResizedEvent } from 'angular-resize-event';
import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Options } from 'src/app/models/options';
import { Position } from 'src/app/models/position';
import { Theme } from 'src/app/models/theme';
import { DuplicateValidator } from 'src/app/validators/board/duplicate-validator';
import { SelectedAreaMask } from 'src/app/validators/board/selected-area-mask';
import { SolutionValidator } from 'src/app/validators/board/solution-validator';
import { BackgroundLayer } from './layers/background-layer';
import { DigitLayer } from './layers/digit-layer';
import { GridLayer } from './layers/grid-layer';

@Component({
  selector: 'app-board-canvas',
  templateUrl: './board-canvas.component.html',
  styleUrls: ['./board-canvas.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoardCanvasComponent implements AfterViewInit, OnChanges {
  @Input()
  board: Board;
  @Input()
  selectedPosition: Position = null;
  @Input()
  options: Options;
  @Input()
  theme: Theme;

  @Output()
  selectedPositionChange: EventEmitter<Position> = new EventEmitter<Position>();

  @ViewChild('canvas')
  canvas: ElementRef;

  duplicateValidator = new DuplicateValidator();
  solutionValidator = new SolutionValidator();
  selectedAreaMask = new SelectedAreaMask();

  private context: CanvasRenderingContext2D = null;
  private backgroundLayer: BackgroundLayer;
  private digitLayer: DigitLayer;
  private gridLayer: GridLayer;

  private canvasSize: number;
  private boardSize: number;
  private cellSize: number;
  private linesPositions = Array(8).fill(0);
  private cellsPositions = Array(9).fill(0);

  constructor() {
    this.backgroundLayer = new BackgroundLayer();
    this.digitLayer = new DigitLayer();
    this.gridLayer = new GridLayer();
  }

  ngAfterViewInit() {
    this.context = this.canvas.nativeElement.getContext('2d');
    const baseConfig = {
      context: this.context,
      board: this.board,
      selectedAreaMask: this.selectedAreaMask,
      solutionValidator: this.solutionValidator,
      duplicateValidator: this.duplicateValidator,
    };
    this.backgroundLayer.updateOptions(baseConfig);
    this.digitLayer.updateOptions(baseConfig);
    this.gridLayer.updateOptions(baseConfig);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.board || changes.selectedPosition) {
      this.selectedAreaMask.update(this.board, this.selectedPosition);
    }

    if (changes.board) {
      this.duplicateValidator.updateValidity(this.board);
      this.solutionValidator.updateValidity(this.board);
      this.backgroundLayer.updateOptions({ board: this.board });
      this.digitLayer.updateOptions({ board: this.board });
    }

    if (changes.selectedPosition) {
      this.backgroundLayer.updateOptions({ selectedPosition: this.selectedPosition });
      this.digitLayer.updateOptions({ selectedPosition: this.selectedPosition });
      this.gridLayer.updateOptions({ selectedPosition: this.selectedPosition });
    }

    if (changes.options) {
      this.backgroundLayer.updateOptions({ options: this.options });
      this.digitLayer.updateOptions({ options: this.options });
    }

    if (changes.theme) {
      this.backgroundLayer.updateOptions({ theme: this.theme });
      this.digitLayer.updateOptions({ theme: this.theme });
      this.gridLayer.updateOptions({ theme: this.theme });
    }

    if (this.context !== null) {
      this.draw();
    }
  }

  onClick(event) {
    const clickedCellPosition = this.calculateClickedCellPosition(event);
    this.selectedPositionChange.emit(clickedCellPosition);
  }

  onResized(event: ResizedEvent) {
    this.recalculateSizes(event.newWidth);
    this.recalculatePositions();

    this.backgroundLayer.updateOptions({ cellSize: this.cellSize, cellPositions: this.cellsPositions });
    this.digitLayer.updateOptions({ cellSize: this.cellSize, cellPositions: this.cellsPositions });
    this.gridLayer.updateOptions({
      boardSize: this.boardSize,
      cellSize: this.cellSize,
      cellPositions: this.cellsPositions,
      linePositions: this.linesPositions,
    });
    this.draw();
  }

  draw() {
    this.context.save();
    this.context.scale(window.devicePixelRatio, window.devicePixelRatio);
    this.context.clearRect(0, 0, this.canvasSize, this.canvasSize);
    this.context.translate(this.theme.padding, this.theme.padding);
    this.backgroundLayer.draw();
    this.digitLayer.draw();
    this.gridLayer.draw();
    this.context.restore();
  }

  private recalculateSizes(canvasSize: number) {
    this.canvasSize = canvasSize;
    const canvasHTMLSize = this.canvasSize * window.devicePixelRatio;
    this.context.canvas.width = canvasHTMLSize;
    this.context.canvas.height = canvasHTMLSize;

    this.boardSize = this.canvasSize - 2 * this.theme.padding;
    this.cellSize = (this.boardSize - 6 * this.theme.thinLineWidth - 2 * this.theme.thickLineWidth) / 9;
  }

  private recalculatePositions() {
    this.cellsPositions[0] = this.cellSize / 2;
    for (let i = 0; i < BoardSizes.size - 1; i++) {
      if (i === 2 || i === 5) {
        this.cellsPositions[i + 1] = this.cellsPositions[i] + this.cellSize + this.theme.thickLineWidth;
      } else {
        this.cellsPositions[i + 1] = this.cellsPositions[i] + this.cellSize + this.theme.thinLineWidth;
      }
      this.linesPositions[i] = (this.cellsPositions[i + 1] + this.cellsPositions[i]) / 2;
    }
  }

  private calculateClickedCellPosition(clickEvent: { offsetX: number; offsetY: number }) {
    const boundingBox = this.canvas.nativeElement.getBoundingClientRect();
    const factor = this.canvasSize / (boundingBox.right - boundingBox.left);
    const position = {
      row: clickEvent.offsetY * factor - this.theme.padding,
      col: clickEvent.offsetX * factor - this.theme.padding,
    };
    position.col = this.linesPositions.findIndex((linePos) => linePos > position.col);
    position.row = this.linesPositions.findIndex((linePos) => linePos > position.row);

    if (position.col === -1) {
      position.col = BoardSizes.size - 1;
    }
    if (position.row === -1) {
      position.row = BoardSizes.size - 1;
    }
    return position;
  }
}
