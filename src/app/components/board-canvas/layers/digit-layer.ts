import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Cell } from 'src/app/models/cell';
import { Options } from 'src/app/models/options';
import { Position } from 'src/app/models/position';
import { Theme } from 'src/app/models/theme';
import { DuplicateValidator } from 'src/app/validators/board/duplicate-validator';
import { SelectedAreaMask, SelectedAreaMaskLevel } from 'src/app/validators/board/selected-area-mask';
import { SolutionValidator } from 'src/app/validators/board/solution-validator';

export class DigitLayer {
  private context: CanvasRenderingContext2D;
  private cellSize: number = null;
  private cellPositions: number[];
  private theme: Theme = null;
  private board: Board;
  private selectedPosition: Position = null;
  private selectedAreaMask: SelectedAreaMask;
  private solutionValidator: SolutionValidator;
  private duplicateValidator: DuplicateValidator;
  private options: Options;

  private valuesFontSize: number;
  private valuesOffset: number;
  private notesFontSize: number;
  private notesOffset: number;

  draw() {
    this.drawValues();
    this.drawNotes();
  }

  updateOptions({
    context = this.context,
    cellSize,
    cellPositions = this.cellPositions,
    theme,
    board = this.board,
    selectedAreaMask = this.selectedAreaMask,
    selectedPosition = this.selectedPosition,
    solutionValidator = this.solutionValidator,
    duplicateValidator = this.duplicateValidator,
    options = this.options,
  }: {
    context?: CanvasRenderingContext2D;
    cellSize?: number;
    cellPositions?: number[];
    theme?: Theme;
    board?: Board;
    selectedAreaMask?: SelectedAreaMask;
    selectedPosition?: Position;
    solutionValidator?: SolutionValidator;
    duplicateValidator?: DuplicateValidator;
    options?: Options;
  }) {
    this.context = context;
    this.cellSize = cellSize !== undefined ? cellSize : this.cellSize;
    this.cellPositions = cellPositions;
    this.theme = theme !== undefined ? theme : this.theme;
    this.board = board;
    this.selectedAreaMask = selectedAreaMask;
    this.selectedPosition = selectedPosition;
    this.solutionValidator = solutionValidator;
    this.duplicateValidator = duplicateValidator;
    this.options = options;

    if ((cellSize !== undefined || theme !== undefined) && this.context !== null && this.cellSize !== null && this.theme !== null) {
      this.recalculateFontSizes();
    }
  }

  private drawValues() {
    this.context.save();
    this.context.font = `${this.valuesFontSize}px ${this.theme.fontFamily}`;
    this.context.textBaseline = 'middle';
    this.context.textAlign = 'center';
    this.context.translate(0, this.valuesOffset);

    const selectedCell = this.selectedPosition !== null ? this.board.cells[this.selectedPosition.row][this.selectedPosition.col] : null;
    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        const position = { row, col };
        const cell = this.board.cells[row][col];
        if (cell.value === null) {
          continue;
        }

        if (this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL) {
          this.context.fillStyle = this.theme.selectedCellFontColor;
        } else if (
          (this.options.highlightSolutionMistake && !this.solutionValidator.validity.getAtPosition(position)) ||
          (this.options.highlightDuplicateMistake && !this.duplicateValidator.validity.getAtPosition(position))
        ) {
          this.context.fillStyle = this.theme.mistakeFontColor;
        } else if (
          this.options.highlightSelectedValues &&
          selectedCell !== null &&
          selectedCell.value !== null &&
          cell.value === selectedCell.value
        ) {
          this.context.fillStyle = this.theme.selectedValueFontColor;
        } else if (cell.locked) {
          this.context.fillStyle = this.theme.lockedCellFontColor;
        } else if (
          this.options.highlightSelectedArea &&
          this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL_AREA
        ) {
          this.context.fillStyle = this.theme.selectedAreaFontColor;
        } else {
          this.context.fillStyle = this.theme.defaultFontColor;
        }

        this.context.fillText(cell.value.toString(), this.cellPositions[position.col], this.cellPositions[position.row]);
      }
    }
    this.context.restore();
  }

  private drawNotes() {
    this.context.save();
    this.context.font = `${this.notesFontSize}px ${this.theme.fontFamily}`;
    this.context.textBaseline = 'middle';
    this.context.textAlign = 'center';
    this.context.translate(0, this.notesOffset);

    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        const position = { row, col };
        const cell = this.board.cells[row][col];
        if (cell.value !== null) {
          continue;
        }

        if (this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL) {
          this.context.fillStyle = this.theme.selectedCellFontColor;
        } else if (
          this.options.highlightSelectedArea &&
          this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL_AREA
        ) {
          this.context.fillStyle = this.theme.selectedAreaFontColor;
        } else {
          this.context.fillStyle = this.theme.defaultFontColor;
        }

        this.drawNotesForCell(cell, position);
      }
    }
    this.context.restore();
  }

  private drawNotesForCell(cell: Cell, position: Position) {
    for (let i = 0; i < 9; i++) {
      if (cell.notes[i]) {
        this.context.fillText(
          (i + 1).toString(),
          this.cellPositions[position.col] + (((i % 3) - 1) / 3) * this.cellSize,
          this.cellPositions[position.row] + ((Math.trunc(i / 3) - 1) / 3) * this.cellSize
        );
      }
    }
  }

  private recalculateFontSizes() {
    ({ fontSize: this.valuesFontSize, offset: this.valuesOffset } = this.recalculateFontSize(this.theme.valueFontSizeFactor));
    ({ fontSize: this.notesFontSize, offset: this.notesOffset } = this.recalculateFontSize(this.theme.noteFontSizeFactor));
  }

  private recalculateFontSize(fontFactor: number): { fontSize: number; offset: number } {
    let fontSize = this.cellSize * fontFactor;
    this.context.save();
    this.context.font = `${fontSize}px ${this.theme.fontFamily}`;
    this.context.textBaseline = 'middle';
    this.context.textAlign = 'center';

    let metrics = this.context.measureText('8');
    const height = metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent;
    fontSize = (fontSize * fontSize) / height;

    this.context.font = `${fontSize}px ${this.theme.fontFamily}`;
    metrics = this.context.measureText('8');
    const offset = (metrics.actualBoundingBoxAscent - metrics.actualBoundingBoxDescent) / 2;

    this.context.restore();

    return { fontSize, offset };
  }
}
