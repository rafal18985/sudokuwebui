import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Options } from 'src/app/models/options';
import { Position } from 'src/app/models/position';
import { Theme } from 'src/app/models/theme';
import { DuplicateValidator } from 'src/app/validators/board/duplicate-validator';
import { SelectedAreaMask, SelectedAreaMaskLevel } from 'src/app/validators/board/selected-area-mask';
import { SolutionValidator } from 'src/app/validators/board/solution-validator';

export class BackgroundLayer {
  private context: CanvasRenderingContext2D;
  private cellSize: number;
  private cellPositions: number[];
  private theme: Theme;
  private board: Board;
  private selectedPosition: Position;
  private selectedAreaMask: SelectedAreaMask;
  private solutionValidator: SolutionValidator;
  private duplicateValidator: DuplicateValidator;
  private options: Options;

  draw() {
    this.context.save();
    const selectedCell = this.selectedPosition !== null ? this.board.cells[this.selectedPosition.row][this.selectedPosition.col] : null;
    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        const position = { row, col };
        const cell = this.board.cells[row][col];
        if (this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL) {
          this.context.fillStyle = this.theme.selectedCellBackgroundColor;
        } else if (
          (this.options.highlightSolutionMistake && !this.solutionValidator.validity.getAtPosition(position)) ||
          (this.options.highlightDuplicateMistake && !this.duplicateValidator.validity.getAtPosition(position))
        ) {
          this.context.fillStyle = this.theme.mistakeBackgroundColor;
        } else if (
          this.options.highlightSelectedArea &&
          this.selectedAreaMask.mask.getAtPosition(position) === SelectedAreaMaskLevel.SELECTED_CELL_AREA
        ) {
          this.context.fillStyle = this.theme.selectedAreaBackgroundColor;
        } else if (
          this.options.highlightSelectedValues &&
          selectedCell !== null &&
          selectedCell.value !== null &&
          cell.value === selectedCell.value
        ) {
          this.context.fillStyle = this.theme.selectedValueBackgroundColor;
        } else if (cell.locked) {
          this.context.fillStyle = this.theme.lockedCellBackgroundColor;
        } else {
          this.context.fillStyle = this.theme.defaultBackgroundColor;
        }

        this.context.fillRect(
          this.cellPositions[position.col] - this.cellSize / 2,
          this.cellPositions[position.row] - this.cellSize / 2,
          this.cellSize,
          this.cellSize
        );
      }
    }

    this.context.restore();
  }

  updateOptions({
    context = this.context,
    cellSize = this.cellSize,
    cellPositions = this.cellPositions,
    theme = this.theme,
    board = this.board,
    selectedPosition = this.selectedPosition,
    selectedAreaMask = this.selectedAreaMask,
    solutionValidator = this.solutionValidator,
    duplicateValidator = this.duplicateValidator,
    options = this.options,
  }: {
    context?: CanvasRenderingContext2D;
    cellSize?: number;
    cellPositions?: number[];
    theme?: Theme;
    board?: Board;
    selectedPosition?: Position;
    selectedAreaMask?: SelectedAreaMask;
    solutionValidator?: SolutionValidator;
    duplicateValidator?: DuplicateValidator;
    options?: Options;
  }) {
    this.context = context;
    this.cellSize = cellSize;
    this.cellPositions = cellPositions;
    this.theme = theme;
    this.board = board;
    this.selectedPosition = selectedPosition;
    this.selectedAreaMask = selectedAreaMask;
    this.solutionValidator = solutionValidator;
    this.duplicateValidator = duplicateValidator;
    this.options = options;
  }
}
