import { Position } from 'src/app/models/position';
import { Theme } from 'src/app/models/theme';

export class GridLayer {
  private context: CanvasRenderingContext2D;
  private boardSize: number;
  private cellSize: number;
  private cellPositions: number[];
  private linePositions: number[];
  private theme: Theme;
  private selectedPosition: Position = null;

  draw() {
    this.context.save();
    this.drawGrid();
    this.drawSelected();
    this.context.restore();
  }

  updateOptions({
    context = this.context,
    boardSize = this.boardSize,
    cellSize = this.cellSize,
    cellPositions = this.cellPositions,
    linePositions = this.linePositions,
    theme = this.theme,
    selectedPosition = this.selectedPosition,
  }: {
    context?: CanvasRenderingContext2D;
    boardSize?: number;
    cellSize?: number;
    cellPositions?: number[];
    linePositions?: number[];
    theme?: Theme;
    selectedPosition?: Position;
  }) {
    this.context = context;
    this.boardSize = boardSize;
    this.cellSize = cellSize;
    this.cellPositions = cellPositions;
    this.linePositions = linePositions;
    this.theme = theme;
    this.selectedPosition = selectedPosition;
  }

  private drawGrid() {
    for (let i = 0; i < this.linePositions.length; i++) {
      if (i === 2 || i === 5) {
        this.context.lineWidth = this.theme.thickLineWidth;
        this.context.strokeStyle = this.theme.thickLineColor;
      } else {
        this.context.lineWidth = this.theme.thinLineWidth;
        this.context.strokeStyle = this.theme.thinLineColor;
      }

      const position = this.linePositions[i];
      this.context.beginPath();
      this.context.moveTo(position, 0);
      this.context.lineTo(position, this.boardSize);

      this.context.moveTo(0, position);
      this.context.lineTo(this.boardSize, position);
      this.context.stroke();
    }
  }

  private drawSelected() {
    if (this.selectedPosition === null) {
      return;
    }

    this.context.strokeStyle = this.theme.selectedCellBorderColor;
    this.context.lineWidth = this.theme.selectedCellLineWidth;
    this.roundRect(
      this.cellPositions[this.selectedPosition.col] - this.cellSize / 2 - this.theme.selectedCellLineWidth / 2,
      this.cellPositions[this.selectedPosition.row] - this.cellSize / 2 - this.theme.selectedCellLineWidth / 2,
      this.cellSize + this.theme.selectedCellLineWidth,
      this.cellSize + this.theme.selectedCellLineWidth,
      this.theme.selectedCellBorderRadius
    );
    this.context.stroke();
  }

  private roundRect(x: number, y: number, w: number, h: number, radius: number) {
    if (w < 2 * radius) {
      radius = w / 2;
    }
    if (h < 2 * radius) {
      radius = h / 2;
    }
    this.context.beginPath();
    this.context.moveTo(x + radius, y);
    this.context.arcTo(x + w, y, x + w, y + h, radius);
    this.context.arcTo(x + w, y + h, x, y + h, radius);
    this.context.arcTo(x, y + h, x, y, radius);
    this.context.arcTo(x, y, x + w, y, radius);
    this.context.closePath();
  }
}
