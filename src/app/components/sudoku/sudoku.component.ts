import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, fromEvent, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, map, pairwise, startWith } from 'rxjs/operators';
import { AutoFillNotes } from 'src/app/commands/auto-fill-notes';
import { Clear } from 'src/app/commands/clear';
import { SelectPosition } from 'src/app/commands/select-position';
import { ToogleNotesMode } from 'src/app/commands/toogle-notes-mode';
import { ToogleNumber } from 'src/app/commands/toogle-number';
import { Level } from 'src/app/entities/level';
import { Options } from 'src/app/models/options';
import { Position } from 'src/app/models/position';
import { Theme } from 'src/app/models/theme';
import { LevelService } from 'src/app/services/level.service';
import { OptionsService } from 'src/app/services/options.service';
import { SudokuState, SudokuStateModel } from 'src/app/state/sudoku-state';
import { themes } from 'src/app/themes/themes';
import { Stopwatch } from 'src/app/utils/stopwatch';
import { UndoRedoStack } from 'src/app/utils/undo-redo-stack';
import { SolutionValidator } from 'src/app/validators/board/solution-validator';
import { CongratulationDialogComponent } from '../congratulation-dialog/congratulation-dialog.component';
import { OptionsDialogComponent } from '../options-dialog/options-dialog.component';

@Component({
  selector: 'app-sudoku',
  templateUrl: './sudoku.component.html',
  styleUrls: ['./sudoku.component.scss'],
})
export class SudokuComponent implements OnInit, OnDestroy {
  level: Level = null;
  sudokuState: SudokuStateModel = null;
  solutionValidator = new SolutionValidator();
  isFinished$: Observable<boolean>;
  isFinished = false;
  options: Options;
  theme: Theme;
  undoRedoStack: UndoRedoStack<SudokuState> = new UndoRedoStack();
  stopwatch: Stopwatch = new Stopwatch();
  forceNoteIfEmpty = false;

  private visibilityState$: Observable<VisibilityState>;
  private subscription: Subscription;

  constructor(
    private router: Router,
    private state: SudokuState,
    private dialog: MatDialog,
    private optionsService: OptionsService,
    private levelService: LevelService,
    private activateRoute: ActivatedRoute
  ) {}

  async ngOnInit() {
    this.subscription = this.optionsService.options$.subscribe((options) => {
      this.options = options;
      this.theme = themes[options.selectedThemeIndex];
    });

    const levelId = Number(this.activateRoute.snapshot.paramMap.get('levelId'));
    if (isNaN(levelId)) {
      this.navigateToMenu();
      return;
    }
    try {
      await this.loadLevel(levelId);
    } catch (error) {
      this.navigateToMenu();
      return;
    }

    this.visibilityState$ = fromEvent(document, 'visibilitychange').pipe(
      map(() => document.visibilityState),
      startWith(document.visibilityState)
    );
    this.isFinished$ = this.state.state$.pipe(
      map((stateModel) => {
        this.solutionValidator.updateValidity(stateModel.board);
        if (
          stateModel.board.cells.every((row) => row.every((cell) => cell.value !== null)) &&
          this.solutionValidator.validity.mask.every((row) => row.every((value) => value))
        ) {
          return true;
        } else {
          return false;
        }
      }),
      distinctUntilChanged()
    );

    this.subscription.add(
      this.state.state$.subscribe((stateModel) => {
        this.sudokuState = stateModel;
      })
    );

    this.subscription.add(
      combineLatest([this.isFinished$, this.visibilityState$]).subscribe(async ([isFinished, visibilityState]) => {
        if (visibilityState === 'hidden') {
          this.stopwatch.pause();
          await this.saveLevel();
        } else if (isFinished) {
          this.stopwatch.pause();
        } else {
          this.stopwatch.start();
        }
      })
    );

    this.subscription.add(
      this.isFinished$.subscribe((isFinished) => {
        if (isFinished) {
          this.levelService.setCurrentLevelId(null);
          this.selectCell(null);
        } else {
          this.levelService.setCurrentLevelId(this.level.id);
        }
        this.isFinished = isFinished;
      })
    );
    this.subscription.add(
      this.isFinished$.pipe(pairwise()).subscribe(([isFinishedPrev, isFinishedCurrent]) => {
        if (isFinishedCurrent && !isFinishedPrev) {
          this.openCongratulationsDialog();
        }
      })
    );
  }

  async ngOnDestroy() {
    await this.saveLevel();
    this.subscription?.unsubscribe();
  }

  @HostListener('window:beforeunload')
  async onBeforeUnload() {
    await this.saveLevel();
  }

  async loadLevel(levelId: number) {
    this.level = await this.levelService.getById(levelId);
    if (this.level.currentState === undefined || this.level.currentState === null) {
      this.state.setState({
        board: this.levelService.convertLevelToBoard(this.level),
        selectedPosition: null,
      });
      this.stopwatch.reset();
    } else {
      this.state.setState({
        board: { ...this.levelService.convertLevelToBoard(this.level), cells: JSON.parse(this.level.currentState) },
        selectedPosition: null,
      });
      this.stopwatch.reset();
      this.stopwatch.addTime({ milliseconds: this.level.elapsedTime });
    }
  }

  async saveLevel() {
    if (this.level === null) {
      return;
    }

    this.levelService.updateUsingState(this.level.id, this.sudokuState, this.stopwatch.totalMilliseconds);
  }

  reset() {
    this.state.setState({ board: this.levelService.convertLevelToBoard(this.level), selectedPosition: null });
    this.undoRedoStack = new UndoRedoStack();
    this.stopwatch.restart();
  }

  selectCell(position: Position) {
    if (this.isFinished) {
      return;
    }

    const selectPositionCommand = new SelectPosition(position);
    selectPositionCommand.do(this.state);
    this.forceNoteIfEmpty = false;
  }

  placeNumber(value: number) {
    const toogleCommand = new ToogleNumber(
      this.sudokuState.selectedPosition,
      value,
      this.options.shouldRemoveNotesFromSelectedCellArea,
      this.forceNoteIfEmpty
    );
    this.undoRedoStack.addAndDo(toogleCommand, this.state);
    this.forceNoteIfEmpty = false;
  }

  toogleNotesMode() {
    const toogleNotesModeCommand = new ToogleNotesMode(
      this.sudokuState.selectedPosition,
      this.options.shouldRemoveNotesFromSelectedCellArea
    );
    this.undoRedoStack.addAndDo(toogleNotesModeCommand, this.state);
    const state = this.state.getState();
    if (state.selectedPosition === null) {
      return;
    }
    const cell = state.board.cells[state.selectedPosition.row][state.selectedPosition.col];
    if (cell.value === null && cell.notes.every((note) => !note)) {
      this.forceNoteIfEmpty = !this.forceNoteIfEmpty;
    } else {
      this.forceNoteIfEmpty = false;
    }
  }

  autoFillNotes() {
    const autoFillNotesCommand = new AutoFillNotes(this.sudokuState.selectedPosition);
    this.undoRedoStack.addAndDo(autoFillNotesCommand, this.state);
    this.forceNoteIfEmpty = false;
  }

  clearCell() {
    const clearCommand = new Clear(this.sudokuState.selectedPosition);
    this.undoRedoStack.addAndDo(clearCommand, this.state);
    this.forceNoteIfEmpty = false;
  }

  undo() {
    this.undoRedoStack.undo(this.state);
    this.forceNoteIfEmpty = false;
  }

  redo() {
    this.undoRedoStack.redo(this.state);
    this.forceNoteIfEmpty = false;
  }

  navigateToMenu() {
    this.router.navigate(['/menu']);
  }

  openSettings() {
    this.dialog.open(OptionsDialogComponent, {
      panelClass: 'dialog-background',
    });
  }

  openCongratulationsDialog() {
    this.dialog.open(CongratulationDialogComponent, {
      panelClass: 'dialog-background',
    });
  }
}
