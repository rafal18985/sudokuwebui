import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ResizedEvent } from 'angular-resize-event';

@Component({
  selector: 'app-keep-aspect-ratio',
  templateUrl: './keep-aspect-ratio.component.html',
  styleUrls: ['./keep-aspect-ratio.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeepAspectRatioComponent {
  @Input()
  aspectRatioWidth = 1;
  @Input()
  aspectRatioHeight = 1;

  width: number;
  height: number;

  onResized(event: ResizedEvent) {
    this.keepAspectRatio(event.newWidth, event.newHeight);
  }

  private keepAspectRatio(parentWidth: number, parentHeight: number) {
    const parentAspectRatio = parentWidth / parentHeight;
    const targetAspectRatio = this.aspectRatioWidth / this.aspectRatioHeight;

    if (parentAspectRatio > targetAspectRatio) {
      this.height = parentHeight;
      this.width = parentHeight * targetAspectRatio;
    } else if (parentAspectRatio < targetAspectRatio) {
      this.height = parentWidth / targetAspectRatio;
      this.width = parentWidth;
    } else {
      this.width = parentWidth;
      this.height = parentHeight;
    }
  }
}
