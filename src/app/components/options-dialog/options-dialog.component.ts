import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Options } from 'src/app/models/options';
import { OptionsService } from 'src/app/services/options.service';
import { themes } from 'src/app/themes/themes';

@Component({
  selector: 'app-options-dialog',
  templateUrl: './options-dialog.component.html',
  styleUrls: ['./options-dialog.component.scss'],
})
export class OptionsDialogComponent implements OnDestroy {
  themes = themes;
  form: FormGroup;
  private subscription: Subscription;

  constructor(private formBuilder: FormBuilder, private optionsService: OptionsService) {
    this.form = formBuilder.group(optionsService.getOptions());

    this.form.valueChanges.pipe(debounceTime(100)).subscribe((newOptions: Options) => {
      optionsService.setOptions(newOptions);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
