import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DifficultyLevel, difficultyLevelToEnum } from 'src/app/models/difficulty-level';
import { PackService } from 'src/app/services/pack.service';
import { OptionsDialogComponent } from '../../options-dialog/options-dialog.component';

@Component({
  selector: 'app-pack-selection',
  templateUrl: './pack-selection.component.html',
  styleUrls: ['./pack-selection.component.scss'],
})
export class PackSelectionComponent implements OnDestroy {
  packs: { id: number; difficultyLevel: DifficultyLevel; elapsedTime: number; solvedPercent: number }[] = [];

  private subscription: Subscription;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private dialog: MatDialog, private packService: PackService) {
    this.subscription = this.activatedRoute.paramMap.subscribe(async (params) => {
      const difficultyLevelString = params.get('difficultyLevel');
      this.packs = await packService.getByDifficultyLevelWithProgress(difficultyLevelToEnum(difficultyLevelString));
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  goBack() {
    this.router.navigate(['/difficultyLevelSelection']);
  }

  goToPack(packId: number) {
    this.router.navigate([`/levelSelection/${this.activatedRoute.snapshot.paramMap.get('difficultyLevel')}/${packId}`]);
  }

  openSettings() {
    const dialogRef = this.dialog.open(OptionsDialogComponent, {
      panelClass: 'dialog-background',
    });
  }
}
