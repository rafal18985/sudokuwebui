import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Level } from 'src/app/entities/level';
import { LevelService } from 'src/app/services/level.service';
import { SudokuGenerator } from 'src/app/utils/sudoku-generator';
import { OptionsDialogComponent } from '../../options-dialog/options-dialog.component';

@Component({
  selector: 'app-difficulty-level-selection',
  templateUrl: './difficulty-level-selection.component.html',
  styleUrls: ['./difficulty-level-selection.component.scss'],
})
export class DifficultyLevelSelectionComponent {
  constructor(private router: Router, private dialog: MatDialog, private levelService: LevelService) {}

  async generateLevel() {
    const generator = new SudokuGenerator();
    const { unsolvedString, solutionString } = generator.generateString();

    const generatedLevel: Level = {
      id: 0,
      packId: 0,
      unsolvedData: unsolvedString,
      solution: solutionString,
      currentState: null,
      elapsedTime: 0,
      solvedPercent: 0,
    };
    await this.levelService.update(generatedLevel);

    this.router.navigate(['/game/0']);
  }

  goBack() {
    this.router.navigate(['/menu']);
  }

  openSettings() {
    const dialogRef = this.dialog.open(OptionsDialogComponent, {
      panelClass: 'dialog-background',
    });
  }
}
