import { Component, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Level } from 'src/app/entities/level';
import { LevelService } from 'src/app/services/level.service';
import { OptionsDialogComponent } from '../../options-dialog/options-dialog.component';

@Component({
  selector: 'app-level-selection',
  templateUrl: './level-selection.component.html',
  styleUrls: ['./level-selection.component.scss'],
})
export class LevelSelectionComponent implements OnDestroy {
  levels: Level[] = [];

  private subscription: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private levelService: LevelService
  ) {
    this.subscription = this.activatedRoute.paramMap.subscribe(async (params) => {
      const packId = Number(params.get('packId'));
      this.levels = await levelService.getByPackId(packId);
    });
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }

  selectLevel(level: Level) {
    this.router.navigate([`/game/${level.id}`]);
  }

  goBack() {
    this.router.navigate([`/packSelection/${this.activatedRoute.snapshot.paramMap.get('difficultyLevel')}`]);
  }

  openSettings() {
    const dialogRef = this.dialog.open(OptionsDialogComponent, {
      panelClass: 'dialog-background',
    });
  }
}
