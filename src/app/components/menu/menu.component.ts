import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { LevelService } from 'src/app/services/level.service';
import { OptionsDialogComponent } from '../options-dialog/options-dialog.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  canResume = false;
  currentLevel: number = null;

  constructor(private router: Router, private dialog: MatDialog, private levelService: LevelService) {}
  async ngOnInit() {
    this.currentLevel = await this.levelService.getCurrentLevelId();
  }

  newGame() {
    this.router.navigate(['/difficultyLevelSelection']);
  }

  resume() {
    if (this.currentLevel === null) {
      return;
    }

    this.router.navigate([`game/${this.currentLevel}`]);
  }

  openSettings() {
    const dialogRef = this.dialog.open(OptionsDialogComponent, {
      panelClass: 'dialog-background',
    });
  }
}
