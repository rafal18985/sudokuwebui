import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ResizedEvent } from 'angular-resize-event';

@Component({
  selector: 'app-height-dependent-font-size',
  templateUrl: './height-dependent-font-size.component.html',
  styleUrls: ['./height-dependent-font-size.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeightDependentFontSizeComponent {
  @Input()
  factor = 1;
  @Input()
  parentHeightOffset = 0;

  fontSize = 14;

  onResized(event: ResizedEvent) {
    this.fontSize = (event.newHeight + this.parentHeightOffset) * this.factor;
  }
}
