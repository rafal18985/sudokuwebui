import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Array2dUtils } from 'src/app/utils/array-2d-utils';
import { DigitPadMask } from './digit-pad-mask';

export class UsedDigits {
  mask: DigitPadMask<number> = new DigitPadMask(0);

  public update(board: Board): void {
    this.mask = new DigitPadMask(0);
    Array2dUtils.runRowWise(
      (position) => {
        const value = board.cells[position.row][position.col].value;
        if (value !== null) {
          this.mask.setAtIndex(value - 1, this.mask.getAtIndex(value - 1) + 1);
        }
      },
      BoardSizes.size,
      BoardSizes.size
    );
  }
}
