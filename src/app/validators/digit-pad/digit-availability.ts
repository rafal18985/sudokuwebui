import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Position } from 'src/app/models/position';
import { Array2dUtils } from 'src/app/utils/array-2d-utils';
import { SubSquareUtils } from 'src/app/utils/sub-square-utils';
import { DigitPadMask } from './digit-pad-mask';

export class DigitAvailability {
  mask: DigitPadMask<boolean> = new DigitPadMask(true);

  public update(board: Board, selectedCell: Position): void {
    this.mask = new DigitPadMask(true);
    if (selectedCell === null) {
      return;
    }

    const subSquarePosition = SubSquareUtils.calculateSubSquarePosition(selectedCell, BoardSizes.squareSize, BoardSizes.squareSize);

    const rowPositions = Array2dUtils.generatePositionsForRow(selectedCell.row, BoardSizes.size);
    const colPositions = Array2dUtils.generatePositionsForCol(selectedCell.col, BoardSizes.size);
    const subSquarePositions = Array2dUtils.generatePositionsForSubSquare(
      subSquarePosition.row,
      subSquarePosition.col,
      BoardSizes.squareSize,
      BoardSizes.squareSize
    );

    [...rowPositions, ...colPositions, ...subSquarePositions].forEach((position) => {
      if (position.row === selectedCell.row && position.col === selectedCell.col) {
        return;
      }
      const value = board.cells[position.row][position.col].value;
      if (value !== null) {
        this.mask.setAtIndex(value - 1, false);
      }
    });
  }
}
