import { BoardSizes } from 'src/app/constants/board-sizes';

export class DigitPadMask<T> {
  mask: T[];

  constructor(defaultValue: T) {
    this.mask = Array(BoardSizes.size).fill(defaultValue);
  }

  getAtIndex(index: number): T {
    return this.mask[index];
  }

  setAtIndex(index: number, value: T): void {
    this.mask[index] = value;
  }
}
