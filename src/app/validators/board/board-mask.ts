import { BoardSizes } from 'src/app/constants/board-sizes';
import { Position } from 'src/app/models/position';

export class BoardMask<T> {
  mask: T[][];

  constructor(defaultValue: T) {
    this.mask = [...Array(BoardSizes.size)].map((_) => Array(BoardSizes.size).fill(defaultValue));
  }

  getAtPosition(position: Position): T {
    return this.mask[position.row][position.col];
  }

  getAtIndex(index: number): T {
    return this.mask[Math.trunc(index / BoardSizes.size)][index % BoardSizes.size];
  }

  setAtPosition(position: Position, value: T): void {
    this.mask[position.row][position.col] = value;
  }

  setAtIndex(index: number, value: T): void {
    this.mask[Math.trunc(index / BoardSizes.size)][index % BoardSizes.size] = value;
  }
}
