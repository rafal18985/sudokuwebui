import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Position } from 'src/app/models/position';
import { Array2dUtils } from 'src/app/utils/array-2d-utils';
import { BoardMask } from './board-mask';

export class DuplicateValidator {
  validity: BoardMask<boolean> = new BoardMask(true);

  public updateValidity(board: Board): void {
    const validityRowWise = this.calculateValidityRowWise(board);
    const validityColumnWise = this.calculateValidityColumnWise(board);
    const validitySquareWise = this.calculateValiditySquareWise(board);

    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        const pos: Position = { row, col };
        this.validity.setAtPosition(
          pos,
          validityRowWise.getAtPosition(pos) && validityColumnWise.getAtPosition(pos) && validitySquareWise.getAtPosition(pos)
        );
      }
    }
  }

  private calculateValidityRowWise(board: Board): BoardMask<boolean> {
    const validity = new BoardMask(true);
    Array2dUtils.runForEachRow(
      (rowPositions) => {
        const duplicates = this.duplicateCheck(rowPositions, board);

        for (let i = 0; i < rowPositions.length; i++) {
          validity.setAtPosition(rowPositions[i], duplicates[i]);
        }
      },
      BoardSizes.size,
      BoardSizes.size
    );

    return validity;
  }

  private calculateValidityColumnWise(board: Board): BoardMask<boolean> {
    const validity = new BoardMask(true);
    Array2dUtils.runForEachCol(
      (colPositions) => {
        const duplicates = this.duplicateCheck(colPositions, board);

        for (let i = 0; i < colPositions.length; i++) {
          validity.setAtPosition(colPositions[i], duplicates[i]);
        }
      },
      BoardSizes.size,
      BoardSizes.size
    );

    return validity;
  }

  private calculateValiditySquareWise(board: Board): BoardMask<boolean> {
    const validity = new BoardMask(true);
    Array2dUtils.runForEachSubSquare(
      (subSquarePositions) => {
        const duplicates = this.duplicateCheck(subSquarePositions, board);

        for (let i = 0; i < subSquarePositions.length; i++) {
          validity.setAtPosition(subSquarePositions[i], duplicates[i]);
        }
      },
      BoardSizes.size,
      BoardSizes.size,
      BoardSizes.squareSize,
      BoardSizes.squareSize
    );

    return validity;
  }

  private duplicateCheck(positions: Position[], board: Board): boolean[] {
    const validity = Array(positions.length).fill(true);

    for (let i = 0; i < positions.length - 1; i++) {
      const position1 = positions[i];
      const currentCell = board.cells[position1.row][position1.col];

      if (currentCell.value === null || validity[i] === false) {
        continue;
      }

      for (let j = i + 1; j < positions.length; j++) {
        const position2 = positions[j];
        const cell2 = board.cells[position2.row][position2.col];

        if (currentCell.value === cell2.value) {
          validity[i] = false;
          validity[j] = false;
        }
      }
    }

    return validity;
  }
}
