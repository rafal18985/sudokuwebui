import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Array2dUtils } from 'src/app/utils/array-2d-utils';
import { BoardMask } from './board-mask';

export class SolutionValidator {
  validity: BoardMask<boolean> = new BoardMask(true);

  public updateValidity(board: Board): void {
    Array2dUtils.runRowWise(
      (position) => {
        if (board.cells[position.row][position.col].value === null) {
          this.validity.setAtPosition(position, true);
        } else {
          this.validity.setAtPosition(
            position,
            board.cells[position.row][position.col].value === board.solution[position.row][position.col]
          );
        }
      },
      BoardSizes.size,
      BoardSizes.size
    );
  }
}
