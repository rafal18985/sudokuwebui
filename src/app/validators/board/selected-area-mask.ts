import { BoardSizes } from 'src/app/constants/board-sizes';
import { Board } from 'src/app/models/board';
import { Position } from 'src/app/models/position';
import { SubSquareUtils } from 'src/app/utils/sub-square-utils';
import { BoardMask } from './board-mask';

export class SelectedAreaMask {
  mask: BoardMask<SelectedAreaMaskLevel> = new BoardMask(SelectedAreaMaskLevel.NONE);

  update(board: Board, selectedPosition: Position) {
    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        if (selectedPosition === null) {
          this.mask.setAtPosition({ row, col }, SelectedAreaMaskLevel.NONE);
        } else if (selectedPosition.row === row && selectedPosition.col === col) {
          this.mask.setAtPosition({ row, col }, SelectedAreaMaskLevel.SELECTED_CELL);
        } else if (
          selectedPosition.row === row ||
          selectedPosition.col === col ||
          SubSquareUtils.inSameSubSquare(selectedPosition, { row, col }, BoardSizes.squareSize, BoardSizes.squareSize)
        ) {
          this.mask.setAtPosition({ row, col }, SelectedAreaMaskLevel.SELECTED_CELL_AREA);
        } else {
          this.mask.setAtPosition({ row, col }, SelectedAreaMaskLevel.NONE);
        }
      }
    }
  }
}

export enum SelectedAreaMaskLevel {
  NONE = 0,
  SELECTED_CELL = 1,
  SELECTED_CELL_AREA = 2,
}
