import { Injectable } from '@angular/core';
import { Pack } from '../entities/pack';
import { DifficultyLevel, difficultyLevelToString } from '../models/difficulty-level';
import { IdbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class PackService {
  constructor(private idbService: IdbService) {}

  async getByDifficultyLevel(difficultyLevel: DifficultyLevel) {
    const difficultyString = difficultyLevelToString(difficultyLevel);

    const packs = await this.idbService.connection.select<Pack>({ from: 'Pack', where: { difficultyLevel: difficultyString } });
    return packs;
  }

  async getByDifficultyLevelWithProgress(
    difficultyLevel: DifficultyLevel
  ): Promise<{ id: number; difficultyLevel: DifficultyLevel; elapsedTime: number; solvedPercent: number }[]> {
    const difficultyString = difficultyLevelToString(difficultyLevel);

    const packs = await this.idbService.connection.select<any>({
      from: 'Pack',
      join: { with: 'Level', on: 'Pack.id = Level.packId' },
      where: { difficultyLevel: difficultyString },

      groupBy: 'packId',
      aggregate: { sum: ['elapsedTime', 'solvedPercent'], count: 'packId' },
    });

    return packs.map((pack) => {
      return {
        id: pack.packId,
        difficultyLevel: pack.difficultyLevel,
        elapsedTime: pack['sum(elapsedTime)'],
        solvedPercent: pack['sum(solvedPercent)'] / pack['count(packId)'],
      };
    });
  }
}
