import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Level } from '../entities/level';
import { Pack } from '../entities/pack';
import { IdbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class LevelImporterService {
  constructor(private idbService: IdbService, private httpClient: HttpClient) {}

  async import() {
    const packs = await this.httpClient.get<Pack[]>('./assets/packs40.json').toPromise();
    const levels = (await this.httpClient.get<Level[]>('./assets/levels40.json').toPromise()).map((level) => {
      return { ...level, elapsedTime: 0, solvedPercent: 0 };
    });

    this.idbService.connection.insert<Pack>({ into: 'Pack', values: packs });
    this.idbService.connection.insert<Level>({ into: 'Level', values: levels });
  }
}
