import { Injectable } from '@angular/core';
import * as JsStore from 'jsstore';
import { DATA_TYPE, IDataBase, ITable } from 'jsstore';
import { environment } from 'src/environments/environment';
declare var require: any;

@Injectable({
  providedIn: 'root',
})
export class IdbService {
  readonly dbName = 'Sudoku';
  connection: JsStore.Connection;
  constructor() {
    this.connection = new JsStore.Connection(new Worker(this.workerPath.default));
  }

  async initJsStore() {
    const schema = this.databaseSchema;
    const isDbCreated = await this.connection.initDb(schema);
    return isDbCreated;
  }

  private get workerPath() {
    if (environment.production) {
      return require('file-loader?name=scripts/[name].[hash].js!jsstore/dist/jsstore.worker.min.js');
    } else {
      return require('file-loader?name=scripts/[name].[hash].js!jsstore/dist/jsstore.worker.js');
    }
  }

  private get databaseSchema() {
    const levelTable: ITable = {
      name: 'Level',
      columns: {
        id: {
          primaryKey: true,
          autoIncrement: true,
        },
        packId: {
          notNull: true,
          dataType: DATA_TYPE.Number,
        },
        unsolvedData: {
          notNull: true,
          dataType: DATA_TYPE.String,
        },
        solution: {
          notNull: true,
          dataType: DATA_TYPE.String,
        },
        elapsedTime: {
          notNull: true,
          dataType: DATA_TYPE.Number,
          default: 0,
        },
        currentState: {
          notNull: false,
          dataType: DATA_TYPE.String,
          default: null,
        },
        solvedPercent: {
          notNull: true,
          dataType: DATA_TYPE.Number,
          default: 0,
        },
      },
    };

    const packTable: ITable = {
      name: 'Pack',
      columns: {
        id: {
          primaryKey: true,
          autoIncrement: true,
        },
        difficultyLevel: {
          notNull: true,
          dataType: DATA_TYPE.String,
        },
      },
    };

    const dataBase: IDataBase = {
      name: this.dbName,
      tables: [levelTable, packTable],
    };
    return dataBase;
  }
}
