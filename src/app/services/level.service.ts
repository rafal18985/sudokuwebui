import { Injectable } from '@angular/core';
import { BoardSizes } from '../constants/board-sizes';
import { Level } from '../entities/level';
import { Board } from '../models/board';
import { SudokuStateModel } from '../state/sudoku-state';
import { IdbService } from './idb.service';

@Injectable({
  providedIn: 'root',
})
export class LevelService {
  tableName = 'Level';

  constructor(private idbService: IdbService) {}

  async getByPackId(packId: number) {
    return await this.idbService.connection.select<Level>({ from: this.tableName, where: { packId } });
  }

  async getById(id: number) {
    const levels = await this.idbService.connection.select<Level>({ from: this.tableName, where: { id } });
    if (levels.length === 1) {
      return levels[0];
    } else {
      return null;
    }
  }

  async getCurrentLevelId() {
    const currentLevel: number = await this.idbService.connection.get('currentLevel');
    return currentLevel;
  }

  async setCurrentLevelId(levelId: number) {
    await this.idbService.connection.set('currentLevel', levelId);
  }

  async update(level: Level) {
    await this.idbService.connection.insert<Level>({ into: this.tableName, values: [level], upsert: true, return: true });
  }

  async updateUsingState(id: number, state: SudokuStateModel, elapsedTime: number) {
    const level = await this.getById(id);
    level.elapsedTime = elapsedTime;
    level.currentState = JSON.stringify(state.board.cells);

    let notLockedCount = 0;
    let filledValuesCount = 0;
    for (let row = 0; row < BoardSizes.size; row++) {
      for (let col = 0; col < BoardSizes.size; col++) {
        const cell = state.board.cells[row][col];
        if (cell.locked) {
          continue;
        }
        notLockedCount++;
        if (cell.value != null) {
          filledValuesCount++;
        }
      }
    }

    level.solvedPercent = (filledValuesCount * 100) / notLockedCount;
    this.update(level);
  }

  convertLevelToBoard(level: Level): Board {
    const board: Board = { cells: [], solution: [] };
    for (let row = 0; row < BoardSizes.size; row++) {
      board.cells[row] = [];
      board.solution[row] = [];
      for (let col = 0; col < BoardSizes.size; col++) {
        board.cells[row][col] = { value: null, notes: Array<boolean>(BoardSizes.size).fill(false), locked: false };
      }
    }

    if (level === null) {
      return board;
    }

    for (let i = 0; i < level.unsolvedData.length; i++) {
      const value = Number(level.unsolvedData[i]);
      const row = Math.trunc(i / BoardSizes.size);
      const col = i % BoardSizes.size;

      if (value !== 0) {
        const cell = board.cells[row][col];
        cell.value = value;
        cell.locked = true;
      }

      board.solution[row][col] = Number(level.solution[i]);
    }

    return board;
  }
}
