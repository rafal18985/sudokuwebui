import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Options } from '../models/options';

@Injectable({
  providedIn: 'root',
})
export class OptionsService {
  options$: Observable<Options>;

  private optionsSubject$: BehaviorSubject<Options>;

  constructor() {
    let options: Options = {
      highlightSolutionMistake: true,
      highlightDuplicateMistake: true,
      highlightAvailableDigits: true,
      highlightSelectedValues: true,
      highlightSelectedArea: true,
      shouldRemoveNotesFromSelectedCellArea: true,
      selectedThemeIndex: 0,
    };

    options = { ...options, ...JSON.parse(localStorage.getItem('options')) };

    this.optionsSubject$ = new BehaviorSubject(options);
    this.options$ = this.optionsSubject$.asObservable();
  }

  getOptions() {
    return this.optionsSubject$.getValue();
  }

  setOptions(options: Options) {
    this.optionsSubject$.next(options);
    localStorage.setItem('options', JSON.stringify(options));
  }
}
