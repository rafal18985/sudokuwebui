import { AfterViewInit, Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { enablePatches } from 'immer';
import { Subscription } from 'rxjs';
import { IdbService } from './services/idb.service';
import { LevelImporterService } from './services/level-importer.service';
import { OptionsService } from './services/options.service';
import { UpdateService } from './services/update.service';
import { themes } from './themes/themes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'SudokuWebUI';

  private subscription: Subscription;

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private updateService: UpdateService,
    private optionsService: OptionsService,
    private renderer: Renderer2,
    private idbService: IdbService,
    private levelImporterService: LevelImporterService
  ) {}

  async ngOnInit() {
    this.matIconRegistry.addSvgIcon('delete', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/delete.svg'));
    this.matIconRegistry.addSvgIcon('edit', this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/edit.svg'));
    enablePatches();

    try {
      const isDbCreated = await this.idbService.initJsStore();
      if (isDbCreated) {
        await this.levelImporterService.import();
      }
    } catch (error) {
      alert(error.message);
    }
  }

  ngAfterViewInit() {
    this.subscription = this.optionsService.options$.subscribe((options) => {
      const theme = themes[options.selectedThemeIndex];
      this.renderer.setStyle(document.body, `--defaultBackgroundColor`, theme.defaultBackgroundColor, 2);
      this.renderer.setStyle(document.body, `--defaultFontColor`, theme.defaultFontColor, 2);
      this.renderer.setStyle(document.body, `--selectedAreaBackgroundColor`, theme.selectedAreaBackgroundColor, 2);
      this.renderer.setStyle(document.body, `--thickLineColor`, theme.thickLineColor, 2);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
