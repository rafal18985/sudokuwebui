import { produceWithPatches } from 'immer';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { SudokuCommand } from './sudoku-command';

export class Clear extends SudokuCommand {
  constructor(private position: Position) {
    super();
  }

  do(state: SudokuState): void {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        return;
      }
      const cell = draft.board.cells[this.position.row][this.position.col];

      if (cell.locked) {
        return;
      }

      cell.value = null;
      cell.notes = Array(9).fill(false);
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);
  }
}
