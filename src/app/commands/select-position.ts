import { produceWithPatches } from 'immer';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { SudokuCommand } from './sudoku-command';

export class SelectPosition extends SudokuCommand {
  constructor(private position: Position) {
    super();
  }

  do(state: SudokuState) {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        draft.selectedPosition = null;
        return;
      }

      const selectedPosition = draft.selectedPosition;
      if (selectedPosition !== null && selectedPosition.row === this.position.row && selectedPosition.col === this.position.col) {
        draft.selectedPosition = null;
      } else {
        draft.selectedPosition = this.position;
      }
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);
  }
}
