import { applyPatches } from 'immer';
import { SudokuState } from '../state/sudoku-state';
import { ICommand } from './i-command';

export abstract class SudokuCommand implements ICommand<SudokuState> {
  protected inversePatches: any[]; // Patch[]
  abstract do(target: SudokuState): void;
  undo(state: SudokuState): void {
    if (this.canUndo) {
      state.setState(applyPatches(state.getState(), this.inversePatches));
    }
  }

  canUndo(): boolean {
    return this.inversePatches !== null && this.inversePatches.length !== 0;
  }
}
