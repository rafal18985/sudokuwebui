import { produceWithPatches } from 'immer';
import { BoardSizes } from '../constants/board-sizes';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { Array2dUtils } from '../utils/array-2d-utils';
import { SubSquareUtils } from '../utils/sub-square-utils';
import { SudokuCommand } from './sudoku-command';

export class RemoveSelectedValueFromNotesInSelectedArea extends SudokuCommand {
  constructor(private position: Position) {
    super();
  }

  do(state: SudokuState): void {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        return;
      }
      const cell = draft.board.cells[this.position.row][this.position.col];
      if (cell.locked) {
        return;
      }
      const value = cell.value;
      if (value === null) {
        return;
      }

      const subSquarePosition = SubSquareUtils.calculateSubSquarePosition(this.position, BoardSizes.squareSize, BoardSizes.squareSize);

      const rowPositions = Array2dUtils.generatePositionsForRow(this.position.row, BoardSizes.size);
      const colPositions = Array2dUtils.generatePositionsForCol(this.position.col, BoardSizes.size);
      const subSquarePositions = Array2dUtils.generatePositionsForSubSquare(
        subSquarePosition.row,
        subSquarePosition.col,
        BoardSizes.squareSize,
        BoardSizes.squareSize
      );

      [...rowPositions, ...colPositions, ...subSquarePositions].forEach((position) => {
        draft.board.cells[position.row][position.col].notes[value - 1] = false;
      });
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);
  }
}
