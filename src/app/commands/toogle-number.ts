import { produceWithPatches } from 'immer';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { RemoveSelectedValueFromNotesInSelectedArea } from './remove-selected-value-from-notes-in-selected-area';
import { SudokuCommand } from './sudoku-command';

export class ToogleNumber extends SudokuCommand {
  private removeNotesCommand: RemoveSelectedValueFromNotesInSelectedArea;

  constructor(
    private position: Position,
    private value: number,
    private shouldRemoveNotesFromSelectedCellArea: boolean,
    private forceNoteIfEmpty: boolean
  ) {
    super();
  }

  do(state: SudokuState): void {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        return;
      }
      const cell = draft.board.cells[this.position.row][this.position.col];
      if (cell.locked) {
        return;
      }
      const notesCount = cell.notes.reduce((count, note) => (note === true ? count + 1 : count), 0);

      if (notesCount === 0) {
        if (cell.value === null) {
          if (this.forceNoteIfEmpty) {
            cell.notes[this.value - 1] = true;
          } else {
            cell.value = this.value;
          }
        } else {
          cell.notes[cell.value - 1] = true;
          cell.notes[this.value - 1] = true;
          cell.value = null;
        }
      } else {
        cell.notes[this.value - 1] = !cell.notes[this.value - 1];
        const newNotesCount = cell.notes.reduce((count, note) => (note === true ? count + 1 : count), 0);
        if (newNotesCount === 1) {
          const index = cell.notes.findIndex((note) => note === true);
          cell.value = index + 1;
          cell.notes[index] = false;
        } else {
          cell.value = null;
        }
      }
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);

    if (this.shouldRemoveNotesFromSelectedCellArea) {
      this.removeNotesCommand = new RemoveSelectedValueFromNotesInSelectedArea(this.position);
      this.removeNotesCommand.do(state);
    }
  }

  undo(state: SudokuState) {
    this.removeNotesCommand?.undo(state);
    super.undo(state);
  }
}
