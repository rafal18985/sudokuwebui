import { produceWithPatches } from 'immer';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { SudokuCommand } from './sudoku-command';

export class PlaceNumber extends SudokuCommand {
  constructor(private position: Position, private value: number) {
    super();
  }

  do(state: SudokuState): void {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        return;
      }
      const cell = draft.board.cells[this.position.row][this.position.col];

      if (cell.locked) {
        return;
      }
      cell.value = this.value;
      for (let i = 0; i < cell.notes.length; i++) {
        cell.notes[i] = false;
      }
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);
  }
}
