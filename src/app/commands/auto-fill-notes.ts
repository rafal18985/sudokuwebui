import { produceWithPatches } from 'immer';
import { Position } from '../models/position';
import { SudokuState } from '../state/sudoku-state';
import { DigitAvailability } from '../validators/digit-pad/digit-availability';
import { SudokuCommand } from './sudoku-command';

export class AutoFillNotes extends SudokuCommand {
  constructor(private position: Position) {
    super();
  }

  do(state: SudokuState): void {
    const [nextState, , inversePatches] = produceWithPatches(state.getState(), (draft) => {
      if (this.position === null) {
        return;
      }
      const cell = draft.board.cells[this.position.row][this.position.col];
      if (cell.locked) {
        return;
      }

      const digitAvailability = new DigitAvailability();
      digitAvailability.update(draft.board, this.position);

      cell.value = null;
      cell.notes = digitAvailability.mask.mask;
    });
    this.inversePatches = inversePatches;
    state.setState(nextState);
  }
}
