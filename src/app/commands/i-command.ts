export interface ICommand<T> {
  do(target: T): void;
  undo(target: T): void;
  canUndo(): boolean;
}
