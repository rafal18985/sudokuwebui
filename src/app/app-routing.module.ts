import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DifficultyLevelSelectionComponent } from './components/menu/difficulty-level-selection/difficulty-level-selection.component';
import { LevelSelectionComponent } from './components/menu/level-selection/level-selection.component';
import { MenuComponent } from './components/menu/menu.component';
import { PackSelectionComponent } from './components/menu/pack-selection/pack-selection.component';
import { SudokuComponent } from './components/sudoku/sudoku.component';

const routes: Routes = [
  { path: 'game/:levelId', component: SudokuComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'difficultyLevelSelection', component: DifficultyLevelSelectionComponent },
  { path: 'packSelection/:difficultyLevel', component: PackSelectionComponent },
  { path: 'levelSelection/:difficultyLevel/:packId', component: LevelSelectionComponent },
  {
    path: '',
    redirectTo: '/menu',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
